package TestTransAccel;

public class Switch {
	private DecisionNode	base_DecisionNode;
	private String	expr;

	public DecisionNode	getbase_DecisionNode() { return base_DecisionNode; }
	public void setbase_DecisionNode(DecisionNode base_DecisionNode) { this.base_DecisionNode = base_DecisionNode; }
	public String	getexpr() { return expr; }
	public void setexpr(String expr) { this.expr = expr; }
	
}
