package urbanizacja;

public class Street implements InterfaceTesting2, NumbersHaver {
	private int	Numbers;
	private String	Name;

	public int	getNumbers() { return Numbers; }
	public void setNumbers(int Numbers) { this.Numbers = Numbers; }
	public String	getName() { return Name; }
	public void setName(String Name) { this.Name = Name; }
	
	public void isNumber() {
		return !Numbers.empty();
	}

	public void crossing() {
		Name = "skrzyżowanie";
	}

}
